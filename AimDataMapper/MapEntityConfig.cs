﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Aim.DataMapper
{
    public class MapEntityConfig<MapEntityT> where MapEntityT : class
    {
        public PropertyInfo UserId;
        public PropertyInfo DataId;
        public PropertyInfo DataType;
    }

    public static class MapEntityConfigExtentions
    {
        public static IServiceCollection ConfigureMapEntity<MapEntityT>(this IServiceCollection services) where MapEntityT : class
        {
            services.Configure<MapEntityConfig<MapEntityT>>(c =>
            {
                foreach(var prop in typeof(MapEntityT).GetProperties())
                {
                    if(prop.Name.EndsWith("UserId"))
                        c.UserId = prop;
                    else if(prop.Name.EndsWith("DataId"))
                        c.DataId = prop;
                    else if(prop.Name.EndsWith("DataType"))
                        c.DataType = prop;
                }

                if (c.UserId == null)
                    throw new NullReferenceException($"Map entity {typeof(MapEntityT)} must have a member that ends with 'UserId'. This represents the ID of the user.");

                if (c.DataId == null)
                    throw new NullReferenceException($"Map entity {typeof(MapEntityT)} must have a member that ends with 'DataId'. This represents the ID of the data.");

                if (c.DataType == null)
                    throw new NullReferenceException($"Map entity {typeof(MapEntityT)} must have a member that ends with 'DataType'. This represents the type of the data.");
            });

            return services;
        }
    }
}
