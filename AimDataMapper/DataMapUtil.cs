﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Aim.DataMapper
{
    /// <summary>
    /// Utility methods useful for data providers.
    /// </summary>
    public static class DataMapUtil
    {
        private static Dictionary<Type, List<Func<PropertyInfo, bool>>> _customCopyFilters = new Dictionary<Type, List<Func<PropertyInfo, bool>>>();
        private static List<Func<PropertyInfo, bool>> _commonCopyFilters = new List<Func<PropertyInfo, bool>>()
        {
            (prop) => prop.CustomAttributes.Any(a => a.AttributeType == typeof(KeyAttribute)),
            (prop) => prop.CustomAttributes.Any(a => a.AttributeType == typeof(TimestampAttribute)),
            (prop) => prop.GetMethod.ReturnType == typeof(ICollection<>).GetGenericTypeDefinition(),
            (prop) => prop.Name == $"{prop.DeclaringType.Name}Id",
            (prop) => prop.Name == "Timestamp"
        };

        /// <summary>
        /// Adds a filter for <see cref="EFAwareShallowCopy{T}(T, ref T)"/> to determine whether to ignore a field when perfoming
        /// the shallow copy.
        /// </summary>
        /// <typeparam name="T">The type to add the filter for.</typeparam>
        /// <param name="ignoreFilter">The filter function to use.</param>
        public static void AddShadowCopyIgnoreFilter<T>(Func<PropertyInfo, bool> ignoreFilter) where T : class
        {
            if(!_customCopyFilters.ContainsKey(typeof(T)))
                _customCopyFilters[typeof(T)] = new List<Func<PropertyInfo, bool>>();

            _customCopyFilters[typeof(T)].Add(ignoreFilter);
        }

        /// <summary>
        ///     Performs a shallow copy from the source object into the destination, except certain
        ///     important fields are ignored.
        /// </summary>
        /// <remarks>
        ///     Note that this only works for EF entities that have the appropriate data annotations.
        ///     Aka, anything specified using the fluent API is not supported.
        /// 
        ///     Also like with EF, it only picks up properties ("{get; set;}" stuff).
        ///     
        ///     Custom ignore filters can be registered using `DataMapUtil.AddShadowCopyIgnoreFilter`.
        /// 
        /// ---------------------------------------------------------------------------------------------------------------------------------------
        ///     The reason you'd use this, is because the user can provide a non-EF-tracked version of a
        ///     user data object (e.g. UserLoginInfo). If you try to update an existing object, with this non-tracked object
        ///     then you'll find that the best way is to pretty much just to directly copy from the non-tracked object into
        ///     the tracked one(there are other various issues with EF that might get in the way as well).
        ///     
        ///     So, you can manually add the code to copy from the source to the destination, but there's two big issues here:
        ///         - It's tedious (the horror)
        ///         - If the fields of the objects change, there's a chance the code won't be updated to copy over any new fields.
        ///             - Leading to "bugs".
        ///             
        ///     This function tries to fix this issue, in that since it uses reflection to determine which fields to copy,
        ///     so it's always up to date (fixes the second issue). And of course, it's automatic (fixes the first issue).
        ///     
        ///     The main risk of this function is that it might not know a certain pattern of fields to ignore, so it may copy
        ///     over data that it shouldn't. This will either result in data discrepencies, keys being messed up (which shouldn't happen if things
        ///     are properly annotated), or EF yelling at you.
        /// ---------------------------------------------------------------------------------------------------------------------------------------
        /// 
        ///     `destination` is marked as `ref` to make it visible from the call site which of the objects is being
        ///     modified. It serves no other purpose.
        ///     
        ///     The fields that are ingored by default as follows:
        ///         - Any [Key] fields are ignored, since that can cause issues in EF. (and is incorrect behaviour)
        ///         - Any field following to convention of '{TypeName}Id' is treated as a [Key] field.
        ///         - Any [Timestamp] fields are ignored, since again, that can cause issues in EF. (also incorrect behaviour anyway)
        ///         - Similarly, Any fields named 'Timestamp' are ignored.
        ///         - Any fields of type `ICollection` are ignored, since knowing EF it'd try to do something silly.
        /// </remarks>
        /// <typeparam name="T">The type that's being used.</typeparam>
        /// <param name="source">The source to copy from.</param>
        /// <param name="destination">The destiniation to copy to.</param>
        public static void EFAwareShallowCopy<T>(T source, ref T destination) where T : class
        {
            var TType = typeof(T);
            foreach (var prop in TType.GetProperties())
            {
                List<Func<PropertyInfo, bool>> customFilters = null;

                if (_commonCopyFilters.Any(f => f(prop))
                    ||  (   _customCopyFilters.TryGetValue(TType, out customFilters)
                         && customFilters.Any(f => f(prop))
                        )
                   )
                    continue;

                var getter = prop.GetMethod;
                var setter = prop.SetMethod;

                setter.Invoke(destination, new[] { getter.Invoke(source, null) });
            }
        }

        /// <summary>
        /// Given a entity class (<typeparamref name="DataEntityT"/>) and a data id class (<typeparamref name="DataIdClass"/>),
        /// retrieve the data id for the entity class, from the data id class.
        /// </summary>
        /// <remarks>
        /// When we create a generic data provider for them, the providers somehow need to know what their data ids are, because that's how
        /// providers determine what mappings belong to what type of data.
        /// 
        /// To do this, data id classes are used.
        /// 
        /// For every data entity that is being used, there needs to be a corresponding data id inside of the data id class.
        /// 
        /// These data ids are provided in a specific format: `public static int [Exact name of DataEntityT] => [unique data id];`
        /// 
        /// Imagine we have two entities, 'Person', and 'Gender'.
        /// 
        /// We'd then need a data id class of:
        /// ```
        /// public static class MyDataIds
        /// {
        ///     public static int Person => 1;
        ///     public static int Gender => 2;
        /// }
        /// ```
        /// 
        /// There is currently no way to detect multiple types using the same id, so the best thing to do is, for every project using this system,
        /// each project should be given a certain range of ids they can use. 
        /// 
        /// e.g Project1 uses 0-999, project2 uses 1000-1999, etc.
        /// </remarks>
        /// <exception cref="Exception">If there is no data id for the entity class.</exception>
        /// <typeparam name="DataIdClass">The class containing the data ids</typeparam>
        /// <typeparam name="DataEntityT">The entity class</typeparam>
        /// <returns>The data id for the entity class.</returns>
        public static int GetDataId<DataIdClass, DataEntityT>()
        {
            var prop = typeof(DataIdClass).GetProperty(typeof(DataEntityT).Name);
            if(prop == null)
                throw new Exception($"ID class '{typeof(DataIdClass)}' does not have a property for Data entity '{typeof(DataEntityT)}'");

            return (int)prop.GetMethod.Invoke(null, null);
        }
    }
}
