﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Aim.DataMapper
{
    /// <summary>
    /// Info about a data mapping.
    /// </summary>
    public class MappingInfo
    {
        /// <summary>
        /// The primary key for the user entity.
        /// </summary>
        public int UserId;

        /// <summary>
        /// The primary key for the data entity.
        /// </summary>
        public int DataId;

        /// <summary>
        /// The type of data this mapping is for. (This is the 'data id' of DataEntityT type parameters)
        /// </summary>
        public int DataType;
    }

    /// <summary>
    /// Interface for a provider that allows read and write access to the mappings for a database.
    /// </summary>
    /// <remarks>
    /// Data providers are encouraged to use this service, since it provides mostly everything that will be needed.
    /// 
    /// 'UniqueMapping' means a mapping where only a single mapping for each userId-dataType pair can exist.
    /// e.g. SingleValue and SingleReference providers will use unique mappings since each user entity can only have a single
    /// mapped value with those providers.
    /// 
    /// While confusing, the rest of the documentation will usually refer 'dataType' as 'data id' when talking about the
    /// 'DataEntityT' type parameters. Not to be confused with the 'dataId' parameter most functions in this interface use, which is the primary key
    /// of the data entity.
    /// </remarks>
    /// <typeparam name="DatabaseT">The <see cref="DbContext"/> that contains the mapping table.</typeparam>
    public interface IMappingDatabaseProvider<DatabaseT> where DatabaseT : DbContext
    {
        /// <summary>
        /// The underlying <see cref="DbContext"/>.
        /// 
        /// This is here so code doesn't have to inject both the proivder, *and* the context at the same time, they can just use
        /// the same object.
        /// </summary>
        DatabaseT Context { get; }

        /// <summary>
        /// Sets the unique mapping between a user, some data, and also sets the data type.
        /// </summary>
        /// <exception cref="InvalidOperationException">If either userId or dataId are int.MinValue</exception>
        /// <param name="userId">The id of the user.</param>
        /// <param name="dataId">The id of the data.</param>
        /// <param name="dataType">The type of data that <paramref name="dataId"/> refers to.</param>
        Task SetUniqueMappingAsync(int userId, int dataId, int dataType);

        /// <summary>
        /// Determines if the userId-dataType pair only has a single mapping between them.
        /// </summary>
        /// <exception cref="Exception">If there are more than one mappings. This is because this simply should not happen with unique mappings.</exception>
        /// <param name="userId">The id of the user.</param>
        /// <param name="dataType">The id of the data.</param>
        /// <returns>True if there's only a single mapping, false if there's no mapping at all.</returns>
        Task<bool> HasUniqueMappingAsync(int userId, int dataType);

        /// <summary>
        /// Gets the 'dataId' for the given userId-dataType pair.
        /// </summary>
        /// <exception cref="Exception">If more than one mapping of 'dataType' for the given 'userId' exist.</exception>
        /// <param name="userId">The id of the user.</param>
        /// <param name="dataType">The type of data to get the id for.</param>
        /// <returns>Null if no mapping was found for the pair, otherwise the id.</returns>
        Task<int?> GetUniqueMappingDataIdAsync(int userId, int dataType);

        /// <summary>
        /// Gets the <see cref="MappingInfo"/> for the userId-dataType pair.
        /// </summary>
        /// <exception cref="Exception">If more than one mapping of 'dataType' for the given 'userId' exist.</exception>
        /// <param name="userId">The id of the user.</param>
        /// <param name="dataType">The type of data to get the info for.</param>
        /// <returns>Null if no mapping was found for the pair, otherwise the mapping info.</returns>
        Task<MappingInfo> GetUniqueMappingInfoAsync(int userId, int dataType);

        /// <summary>
        /// Gets all of the mapping info that are for a certain type of data.
        /// </summary>
        /// <param name="dataType">The type of data to get the mappings for.</param>
        /// <returns>All mappings for the given data type.</returns>
        IQueryable<MappingInfo> GetAllMappingInfo(int dataType);
    }

    /// <summary>
    /// Please use <see cref="DatabaseProviderExtentions.AddGenericDatabaseProvider{DatabaseT, MapEntityT}(IServiceCollection)"/> instead
    /// of using this class directly.
    /// </summary>
    public class MappingDatabaseProviderGeneric<DatabaseT, MapEntityT> : IMappingDatabaseProvider<DatabaseT> 
    where DatabaseT   : DbContext
    where MapEntityT  : class, new()
    {
        private readonly IOptionsMonitor<MapEntityConfig<MapEntityT>> _mapOptions;
        public DatabaseT Context { get; }

        public MappingDatabaseProviderGeneric(DatabaseT db,
                                              IOptionsMonitor<MapEntityConfig<MapEntityT>> mapOptions)
        {
            this.Context = db;
            this._mapOptions = mapOptions;
        }

        public async Task SetUniqueMappingAsync(int userId, int dataId, int dataType)
        {
            if(userId == int.MinValue)
                throw new InvalidOperationException("Please call SaveChanges() before this function, since the userId hasn't been determined by EF yet.");

            if(dataId == int.MinValue)
                throw new InvalidOperationException("Please call SaveChanges() before this function, since the dataId hasn't been determined by EF yet.");

            var options = this._mapOptions.CurrentValue;
            var map = await this.Context.Set<MapEntityT>().SingleOrDefaultAsync(m => this.Compare(m, userId, null, dataType));
            
            if(map == null)
            {
                map = new MapEntityT();
                await this.Context.Set<MapEntityT>().AddAsync(map);
            }

            options.UserId.SetMethod.Invoke(map, new object[] { userId });
            options.DataId.SetMethod.Invoke(map, new object[] { dataId });
            options.DataType.SetMethod.Invoke(map, new object[] { dataType });
        }

        public async Task<bool> HasUniqueMappingAsync(int userId, int dataType)
        {
            var count = await this.Context.Set<MapEntityT>()
                                          .CountAsync(map => this.Compare(map, userId, null, dataType));
            if(count > 1)
                throw new Exception($"User #{userId} has multiple mappings for data type {dataType}, meaning that somewhere, unique and multiple data providers " +
                                    $"were mixed for a single data type.");

            return count == 1;
        }

        public async Task<int?> GetUniqueMappingDataIdAsync(int userId, int dataType)
        {
            var map = await this.Context.Set<MapEntityT>()
                                        .SingleOrDefaultAsync(m => this.Compare(m, userId, null, dataType));

            return (map == null) ? (int?)null : (int)this._mapOptions.CurrentValue.DataId.GetMethod.Invoke(map, null);
        }

        private bool Compare(MapEntityT map, int userId, int? dataId, int dataType)
        {
            var options = this._mapOptions.CurrentValue;
            // Yes, it's ugly. But try to imagine it in other ways and decide whether this is so bad or not.
            return (int)options.UserId.GetMethod.Invoke(map, null) == userId
                && (   !dataId.HasValue 
                    || (int)options.DataId.GetMethod.Invoke(map, null) == dataId
                   )
                && (int)options.DataType.GetMethod.Invoke(map, null) == dataType;
        }

        public async Task<MappingInfo> GetUniqueMappingInfoAsync(int userId, int dataType)
        {
            var map = await this.Context.Set<MapEntityT>()
                                        .SingleOrDefaultAsync(m => this.Compare(m, userId, null, dataType));

            return (map == null) ? null : new MappingInfo
            {
                // TODO: Make an extention function so we don't have to see giant lines of mess like this anymore.
                DataId = (int)this._mapOptions.CurrentValue.DataId.GetMethod.Invoke(map, null),
                DataType = (int)this._mapOptions.CurrentValue.DataType.GetMethod.Invoke(map, null),
                UserId = (int)this._mapOptions.CurrentValue.UserId.GetMethod.Invoke(map, null)
            };
        }

        public IQueryable<MappingInfo> GetAllMappingInfo(int dataType)
        {
            return this.Context.Set<MapEntityT>()
                               .Where(m => dataType == (int)this._mapOptions.CurrentValue.DataType.GetMethod.Invoke(m, null))
                               .Select(m => new MappingInfo
                               {
                                   // TODO: Make an extention function so we don't have to see giant lines of mess like this anymore.
                                   DataId = (int)this._mapOptions.CurrentValue.DataId.GetMethod.Invoke(m, null),
                                   DataType = (int)this._mapOptions.CurrentValue.DataType.GetMethod.Invoke(m, null),
                                   UserId = (int)this._mapOptions.CurrentValue.UserId.GetMethod.Invoke(m, null)
                               });
        }
    }

    public static class DatabaseProviderExtentions
    {
        /// <summary>
        /// Creates an <see cref="IMappingDatabaseProvider{DatabaseT}"/> for the given <typeparamref name="DatabaseT"/>.
        /// </summary>
        /// <typeparam name="DatabaseT">The <see cref="DbContext"/> being used for mapping.</typeparam>
        /// <typeparam name="MapEntityT">The entity used for storing the mapping data.</typeparam>
        /// <param name="services">The service builder.</param>
        /// <returns><paramref name="services"/> for chaining.</returns>
        public static IServiceCollection AddGenericDatabaseProvider<DatabaseT, MapEntityT>(this IServiceCollection services)
        where DatabaseT   : DbContext
        where MapEntityT  : class, new()
        {
            services.ConfigureMapEntity<MapEntityT>();
            services.AddScoped<IMappingDatabaseProvider<DatabaseT>, MappingDatabaseProviderGeneric<DatabaseT, MapEntityT>>();
            return services;
        }
    }
}
