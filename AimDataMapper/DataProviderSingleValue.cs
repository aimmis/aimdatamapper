﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace Aim.DataMapper
{    /// <summary>
     /// The user does not need to worry about this.
     /// 
     /// This is automatically created via the <see cref="DataProviderSingleValueGenericExtentions.AddGenericSingleValueProvider{MapDatabaseT, DataDatabaseT, UserEntityT, DataEntityT, DataIdClass}(IServiceCollection)"/>
     /// extention function.
     /// </summary>
    public class DataProviderSingleValueGenericConfig<DataEntityT> where DataEntityT : class
    {
        public int DataType;
    }

    /// <summary>
    /// The interface for a SingleValue data provider.
    /// </summary>
    /// <remarks>
    /// Please refer to this project's wiki (on gitlab) to learn about the differences between Values and References.
    /// 
    /// Data providers should use a <see cref="IMappingDatabaseProvider{DatabaseT}"/> to access and modify mapping data.
    /// </remarks>
    /// <typeparam name="UserEntityT">The user entity this provider is for.</typeparam>
    /// <typeparam name="DataEntityT">The data entity this provider is for.</typeparam>
    public interface IDataProviderSingleValue<UserEntityT, DataEntityT>
    where DataEntityT : class
    where UserEntityT : class
    {
        /// <summary>
        /// Sets the value for the given user.
        /// </summary>
        /// <remarks>
        /// Behaviour that should be implemented (can change to your needs, but not recommended):
        ///     - If there is currently no mapping between the user, and the data id for the data entity.
        ///         - *Insert* a new value into the data database for <paramref name="entity"/>.
        ///         - *Insert* a new mapping into the map database between <paramref name="user"/> and <paramref name="entity"/>.
        ///     - If there is currently a mapping between the user.
        ///         - *Update* the existing value in the data database for <paramref name="entity"/>.
        ///             - If no value exists then the behaviour is to be defined by the data provider. Usually this should result in some kind of Exception though.
        ///         - Usually the mapping data doesn't need to be updated, but do so if needed.
        /// </remarks>
        /// <param name="user">The user to set the value for.</param>
        /// <param name="entity">The data to use as the value.</param>
        /// <returns><paramref name="entity"/></returns>
        Task<DataEntityT> SetAsync(UserEntityT user, DataEntityT entity);

        /// <summary>
        /// Retrieves the value for the given user, or returns a default value if no value exists.
        /// </summary>
        /// <remarks>
        /// A value exists for a user only when there's a mapping where the userId is the id of <paramref name="user"/>,
        /// and the dataType is the data id for the <typeparamref name="DataEntityT"/>.
        /// </remarks>
        /// <param name="user">The user to get the value for.</param>
        /// <param name="default_">The data entity to use in the event that no value has been set for this user.</param>
        /// <returns>Either the value set for this user, or <paramref name="default_"/></returns>
        Task<DataEntityT> GetOrDefaultAsync(UserEntityT user, Lazy<DataEntityT> default_ = null);

        /// <summary>
        /// Retrieves the <see cref="MappingInfo"/> between the given user and it's value, or null if no mapping exists.
        /// </summary>
        /// <param name="user">The user to get the mapping for.</param>
        /// <returns>The mapping between the user and it's value, or null.</returns>
        Task<MappingInfo> GetMappingInfoOrNullAsync(UserEntityT user);

        /// <summary>
        /// Retrieves all mapping info for the <typeparamref name="DataEntityT"/>, for all users.
        /// 
        /// This is a niche function with niche applications.
        /// </summary>
        IQueryable<MappingInfo> GetAllMappingInfo();

        /// <summary>
        /// Given a data entity, performs a reverse lookup in the mapping info to find the user id belonging
        /// to the given data entity.
        /// </summary>
        /// <param name="entity">The entity to get the user of.</param>
        /// <returns>Null if the entity has no mapping (which is likely a bug on your side), or the id of the user that has a mapping to the entity.</returns>
        Task<int?> ReverseLookupUserId(DataEntityT entity);
    }

    public class DataProviderSingleValueGeneric<MapDatabaseT, DataDatabaseT, UserEntityT, DataEntityT> : IDataProviderSingleValue<UserEntityT, DataEntityT> 
    where MapDatabaseT : DbContext
    where DataDatabaseT : DbContext
    where DataEntityT : class
    where UserEntityT : class
    {
        readonly IMappingDatabaseProvider<MapDatabaseT> _mapDb;
        readonly DataDatabaseT _dataDb;
        readonly IOptionsMonitor<DataProviderSingleValueGenericConfig<DataEntityT>> _options;
        readonly IOptionsMonitor<DataEntityConfig<DataEntityT>> _dataOptions;
        readonly IOptionsMonitor<DataEntityConfig<UserEntityT>> _userOptions;

        public DataProviderSingleValueGeneric(IMappingDatabaseProvider<MapDatabaseT> mapDb,
                                              DataDatabaseT dataDb,
                                              IOptionsMonitor<DataProviderSingleValueGenericConfig<DataEntityT>> options,
                                              IOptionsMonitor<DataEntityConfig<DataEntityT>> dataOptions,
                                              IOptionsMonitor<DataEntityConfig<UserEntityT>> userOptions)
        {
            this._dataDb = dataDb;
            this._mapDb = mapDb;
            this._options = options;
            this._dataOptions = dataOptions;
            this._userOptions = userOptions;
        }

        public Task<MappingInfo> GetMappingInfoOrNullAsync(UserEntityT user)
        {
            var userId = (int)this._userOptions.CurrentValue.PrimaryKey.GetMethod.Invoke(user, null);
            return this._mapDb.GetUniqueMappingInfoAsync(userId, this._options.CurrentValue.DataType);
        }

        public async Task<DataEntityT> SetAsync(UserEntityT user, DataEntityT entity)
        {
            var userId = (int)this._userOptions.CurrentValue.PrimaryKey.GetMethod.Invoke(user, null);
            if(!await this._mapDb.HasUniqueMappingAsync(userId, this._options.CurrentValue.DataType))
            {
                this._dataDb.Add(entity);
                await this._dataDb.SaveChangesAsync(); // This will give 'entity' an Id. Unavoidable save unfortunately.

                var dataId = (int)this._dataOptions.CurrentValue.PrimaryKey.GetMethod.Invoke(entity, null);
                await this._mapDb.SetUniqueMappingAsync(userId, dataId, this._options.CurrentValue.DataType);
                await this._mapDb.Context.SaveChangesAsync();

                return entity;
            }
            else
            {
                var dataId = (int)this._dataOptions.CurrentValue.PrimaryKey.GetMethod.Invoke(entity, null);
                var dbEntity = await this._dataDb.FindAsync<DataEntityT>(dataId);
                DataMapUtil.EFAwareShallowCopy(entity, ref dbEntity);
                await this._dataDb.SaveChangesAsync();

                return dbEntity;
            }
        }

        public async Task<DataEntityT> GetOrDefaultAsync(UserEntityT user, Lazy<DataEntityT> default_ = null)
        {
            var userId = (int)this._userOptions.CurrentValue.PrimaryKey.GetMethod.Invoke(user, null);
            var dataId = await this._mapDb.GetUniqueMappingDataIdAsync(userId, this._options.CurrentValue.DataType);

            if (dataId == null)
                return default_?.Value ?? null;

            var value = await this._dataDb.Set<DataEntityT>().FindAsync(dataId);
            return value ?? default_.Value;
        }

        public IQueryable<MappingInfo> GetAllMappingInfo()
        {
            return this._mapDb.GetAllMappingInfo(this._options.CurrentValue.DataType);
        }

        public async Task<int?> ReverseLookupUserId(DataEntityT entity)
        {
            var map = await this.GetAllMappingInfo().FirstOrDefaultAsync(i => i.DataType == this._options.CurrentValue.DataType
                                                                           && i.DataId == (int)this._dataOptions.CurrentValue.PrimaryKey.GetMethod.Invoke(entity, null));

            return map?.UserId;
        }
    }

    public static class DataProviderSingleValueGenericExtentions
    {        
        /// <summary>
        /// Extention method to easily setup a <see cref="DataProviderSingleValueGeneric{DataDatabaseT, MapDatabaseT, UserEntityT, DataEntityT}"/>.
        /// </summary>
        /// <remarks>
        /// This function will call <see cref="DataEntityConfigExtentions.ConfigureDataEntity{DataEntityT}(IServiceCollection)"/> on both
        /// <typeparamref name="DataEntityT"/> and <typeparamref name="UserEntityT"/>.
        /// 
        /// This function will create the <see cref="DataProviderSingleValueGenericConfig{DataEntityT}"/> for the provider.
        /// </remarks>
        public static IServiceCollection AddGenericSingleValueProvider<MapDatabaseT, DataDatabaseT, UserEntityT, DataEntityT, DataIdClass>(this IServiceCollection services)
        where MapDatabaseT : DbContext
        where DataDatabaseT : DbContext
        where DataEntityT : class
        where UserEntityT : class
        {
            services.ConfigureDataEntity<DataEntityT>()
                    .ConfigureDataEntity<UserEntityT>()
                    .AddScoped<IDataProviderSingleValue<UserEntityT, DataEntityT>, 
                               DataProviderSingleValueGeneric<MapDatabaseT, DataDatabaseT, UserEntityT, DataEntityT>>()
                    .Configure<DataProviderSingleValueGenericConfig<DataEntityT>>(c => c.DataType = DataMapUtil.GetDataId<DataIdClass, DataEntityT>());
            return services;
        }
    }
}
