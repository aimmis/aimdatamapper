﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Aim.DataMapper
{
    /// <summary>
    /// This is a helper class used to easily setup data mapping.
    /// </summary>
    /// <typeparam name="MapDatabaseT">The <see cref="DbContext"/> that contains the table for the <typeparamref name="MapEntityT"/></typeparam>
    /// <typeparam name="DataDatabaseT">The <see cref="DbContext"/> that contains the table for the data entities.</typeparam>
    /// <typeparam name="MapEntityT">The entity that is used for keeping track of the data mappings.</typeparam>
    /// <typeparam name="DataIdClass">
    /// The type containing the data ids for all of the data entities to configure.
    /// 
    /// Note that this is only useful for the built-in generic providers.
    /// 
    /// Custom providers may also make use of this I guess, but they're more likely to be more specialised anyway.
    /// 
    /// See <see cref="DataMapUtil.GetDataId{DataIdClass, DataEntityClass}"/> for more info.
    /// </typeparam>
    public class DataMapBuilder<MapDatabaseT, DataDatabaseT, MapEntityT, DataIdClass>
    where MapDatabaseT : DbContext
    where DataDatabaseT : DbContext
    where MapEntityT : class, new()
    where DataIdClass : class
    {
        readonly IServiceCollection _services;
        
        public DataMapBuilder(IServiceCollection services)
        {
            this._services = services;
        }

        /// <summary>
        /// Configures a SingleValue data mapping between <typeparamref name="UserEntityT"/> and <typeparamref name="DataEntityT"/>.
        /// </summary>
        /// <seealso cref="IDataProviderSingleValue{UserEntityT, DataEntityT}"/>
        /// <typeparam name="UserEntityT">The 'user' entity type. This is generally the 'parent' of the mapped data.</typeparam>
        /// <typeparam name="DataEntityT">The 'data' entity type. This is generally the actual data, which is then associated with the 'parent'.</typeparam>
        /// <returns>`this`, to allow chaining</returns>
        public DataMapBuilder<MapDatabaseT, DataDatabaseT, MapEntityT, DataIdClass> UseSingleValue<UserEntityT, DataEntityT>()
        where DataEntityT : class
        where UserEntityT : class
        {
            this._services.AddGenericSingleValueProvider<MapDatabaseT, DataDatabaseT, UserEntityT, DataEntityT, DataIdClass>();

            return this;
        }

        /// <summary>
        /// Configures a SingleReference data mapping between <typeparamref name="UserEntityT"/> and <typeparamref name="DataEntityT"/>.
        /// </summary>
        /// <seealso cref="IDataProviderSingleReference{UserEntityT, DataEntityT}"/>
        /// <typeparam name="UserEntityT">The 'user' entity type. This is generally the 'parent' of the mapped data.</typeparam>
        /// <typeparam name="DataEntityT">The 'data' entity type. This is generally the actual data, which is then associated with the 'parent'.</typeparam>
        /// <returns>`this`, to allow chaining</returns>
        public DataMapBuilder<MapDatabaseT, DataDatabaseT, MapEntityT, DataIdClass> UseSingleReference<UserEntityT, DataEntityT>() 
        where DataEntityT : class
        where UserEntityT : class
        {
            this._services.AddGenericSingleReferenceProvider<DataDatabaseT, MapDatabaseT, UserEntityT, DataEntityT, DataIdClass>();

            return this;
        }

        /// <summary>
        /// Configures a <see cref="IMappingDatabaseProvider{DatabaseT}"/> for the given <typeparamref name="MapDatabaseT"/>
        /// that was passed as a type parameter to this class.
        /// </summary>
        /// <returns>`this`, to allow chaining</returns>
        public DataMapBuilder<MapDatabaseT, DataDatabaseT, MapEntityT, DataIdClass> UseMapDatabase()
        {
            this._services.AddGenericDatabaseProvider<MapDatabaseT, MapEntityT>();

            return this;
        }

        /// <summary>
        /// Creates a <see cref="DataMapService{UserEntityT}"/> for the <typeparamref name="UserEntityT"/>.
        /// 
        /// In short, the DataMapService class makes it easy to access the data providers for a certain user entity type.
        /// </summary>
        /// <typeparam name="UserEntityT">The user entity to configure the DataMapService for.</typeparam>
        /// <returns>`this`, to allow chaining</returns>
        public DataMapBuilder<MapDatabaseT, DataDatabaseT, MapEntityT, DataIdClass> UseUserType<UserEntityT>()
        where UserEntityT : class
        {
            this._services.AddDataMapperService<UserEntityT>();

            return this;
        }
    }
}
