﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Aim.DataMapper
{
    /// <summary>
    /// This service provides an easy way to access the data providers for a given <typeparamref name="UserEntityT"/>.
    /// </summary>
    /// <remarks>
    /// An very simple example usage is to use <see cref="DataMapBuilder{MapDatabaseT, DataDatabaseT, MapEntityT, DataIdClass}.UseSingleValue{UserEntityT, DataEntityT}"/>
    /// to create a <see cref="IDataProviderSingleValue{UserEntityT, DataEntityT}"/>, then use <see cref="DataMapService{UserEntityT}.SingleValue{DataEntityT}"/> to retrieve
    /// it at another point in the program (using the same UserEntityT of course).
    /// </remarks>
    /// <typeparam name="UserEntityT">The user entity to use.</typeparam>
    public class DataMapService<UserEntityT>
    where UserEntityT : class
    {
        readonly IServiceProvider _services;

        public DataMapService(IServiceProvider provider)
        {
            this._services = provider;
        }

        /// <summary>
        /// Retrieves the <see cref="IDataProviderSingleReference{UserEntityT, DataEntityT}"/> of the data entity for this user entity.
        /// </summary>
        /// <exception cref="InvalidOperationException">If no data provider exists for this pair of entity types.</exception>
        /// <typeparam name="DataEntityT">The data entity to get the provider for.</typeparam>
        public IDataProviderSingleReference<UserEntityT, DataEntityT> SingleReference<DataEntityT>()
        where DataEntityT : class
        {
            return this._services.GetRequiredService<IDataProviderSingleReference<UserEntityT, DataEntityT>>();
        }

        /// <summary>
        /// Retrieves the <see cref="IDataProviderSingleValue{UserEntityT, DataEntityT}"/> of the data entity for this user entity.
        /// </summary>
        /// <exception cref="InvalidOperationException">If no data provider exists for this pair of entity types.</exception>
        /// <typeparam name="DataEntityT">The data entity to get the provider for.</typeparam>
        public IDataProviderSingleValue<UserEntityT, DataEntityT> SingleValue<DataEntityT>()
        where DataEntityT : class
        {
            return this._services.GetRequiredService<IDataProviderSingleValue<UserEntityT, DataEntityT>>();
        }
    }

    public static class DataMapServiceExtentions
    {
        /// <summary>
        /// Extention method to easily create a <see cref="DataMapService{UserEntityT}"/> for the given <typeparamref name="UserEntityT"/>.
        /// </summary>
        /// <typeparam name="UserEntityT">The user entity to create the service for.</typeparam>
        /// <param name="services">The service builder.</param>
        /// <returns><paramref name="services"/> for chaining.</returns>
        public static IServiceCollection AddDataMapperService<UserEntityT>(this IServiceCollection services)
        where UserEntityT : class
        {
            services.AddScoped<DataMapService<UserEntityT>>();
            return services;
        }
    }
}
