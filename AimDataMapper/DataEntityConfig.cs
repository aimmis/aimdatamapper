﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Microsoft.Extensions.DependencyInjection;

namespace Aim.DataMapper
{
    /// <summary>
    /// This config is generated for any types that are passed as the `DataEntityT` type parameter
    /// for the "AddGenericXXXProvider" series of extention methods.
    /// 
    /// The purpose of this class is to allow data providers an easy way to gain access to the primary key property
    /// for the `DataEntityT` type.
    /// </summary>
    /// <remarks>
    /// You can get the config for a data entity via dependency injection, by using `IOptionsMonitor<DataEntityConfig<SomeDataEntityT>>` in
    /// your dependency injected function (usually the constructor).
    /// </remarks>
    /// <typeparam name="DataEntityT"></typeparam>
    public class DataEntityConfig<DataEntityT> where DataEntityT : class
    {
        public PropertyInfo PrimaryKey;
    }

    public static class DataEntityConfigExtentions
    {
        /// <summary>
        /// Creates the <see cref="DataEntityConfig{DataEntityT}"/> for the given data entity.
        /// </summary>
        /// <remarks>
        /// The primary key property is matched in one of the following ways (in order of priority):
        ///     - There is a property formatted as "{Name of DataEntityT}Id", e.g. "PersonId"
        /// </remarks>
        /// <typeparam name="DataEntityT">The data entity to configure.</typeparam>
        /// <param name="services">The service builder.</param>
        /// <returns><paramref name="services"/> so you can chain functions.</returns>
        public static IServiceCollection ConfigureDataEntity<DataEntityT>(this IServiceCollection services) where DataEntityT : class
        {
            services.Configure<DataEntityConfig<DataEntityT>>(c =>
            {
                foreach (var prop in typeof(DataEntityT).GetProperties())
                {
                    if (prop.Name == $"{typeof(DataEntityT).Name}Id")
                        c.PrimaryKey = prop;
                }

                if (c.PrimaryKey == null)
                    throw new NullReferenceException($"Data entity {typeof(DataEntityT)} must have a member that is called '{typeof(DataEntityT).Name}Id'. This must be the primary key for the entity.");
            });

            return services;
        }
    }
}
