using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Aim.DataMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace AimDataMapper.Tests
{
    [TestClass]
    public class SingleValueTests
    {
        [TestMethod]
        public void Test()
        {
            // Create the services.
            var serviceBuilder = new ServiceCollection();
            serviceBuilder.AddEntityFrameworkInMemoryDatabase()
                          .AddDbContext<TestContext>(o =>
                          {
                              o.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                              o.UseInMemoryDatabase("singlevalue");
                          });

            (new DataMapBuilder<TestContext, TestContext, UserDataMap, TestEntityIds>(serviceBuilder))
                .UseMapDatabase()
                .UseUserType<User>()
                .UseSingleValue<User, NameInfo>();

            var scope = serviceBuilder.BuildServiceProvider().CreateScope();
            var services = scope.ServiceProvider;

            // Get the services we need
            var db = services.GetService<TestContext>();
            var map = services.GetService<DataMapService<User>>();
            
            // Create a new user, then set his name
            var user = new User();
            db.Users.Add(user);
            db.SaveChanges();
            
            Assert.AreEqual(1, db.Users.CountAsync().Result);
            Assert.AreEqual(0, db.UserDataMaps.CountAsync().Result);
            Assert.AreEqual(0, db.NameInfos.CountAsync().Result);

            map.SingleValue<NameInfo>().SetAsync(user, new NameInfo{ First = "Bradley", Second = "Chatha" }).Wait();
            Assert.AreEqual(1, db.UserDataMaps.CountAsync().Result);
            Assert.AreEqual(1, db.NameInfos.CountAsync().Result);

            var mapping = map.SingleValue<NameInfo>().GetMappingInfoOrNullAsync(user).Result;
            Assert.AreEqual(1, mapping.UserId);
            Assert.AreEqual(1, mapping.DataId);
            Assert.AreEqual(2, mapping.DataType);

            var name = map.SingleValue<NameInfo>().GetOrDefaultAsync(user).Result;
            Assert.AreEqual("Bradley", name.First);
            Assert.AreEqual("Chatha", name.Second);

            // Perform a reverse lookup
            var lookupId = map.SingleValue<NameInfo>().ReverseLookupUserId(name).Result;
            Assert.AreEqual(user.UserId, lookupId);
        }
    }
}
