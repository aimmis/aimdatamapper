using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Aim.DataMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;

namespace AimDataMapper.Tests
{
    [TestClass]
    public class SingleReferenceTests
    {
        [TestMethod]
        public void Test()
        {
            // Create the services.
            var serviceBuilder = new ServiceCollection();
            serviceBuilder.AddEntityFrameworkInMemoryDatabase()
                          .AddDbContext<TestContext>(o =>
                          {
                              o.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
                              o.UseInMemoryDatabase("singleref");
                          });

            (new DataMapBuilder<TestContext, TestContext, UserDataMap, TestEntityIds>(serviceBuilder))
                .UseMapDatabase()
                .UseUserType<User>()
                .UseSingleReference<User, Gender>();

            var scope = serviceBuilder.BuildServiceProvider().CreateScope();
            var services = scope.ServiceProvider;

            // Get the services we need
            var db = services.GetService<TestContext>();
            var map = services.GetService<DataMapService<User>>();

            // Seed the database with some values.
            db.Genders.Add(new Gender { Description = "Male" });
            db.Genders.Add(new Gender { Description = "Female" });
            db.Genders.Add(new Gender { Description = "Apache Helicopter" });
            
            // Create a new user, then set his gender
            var user = new User();
            db.Users.Add(user);
            db.SaveChanges();

            Assert.AreEqual(3, db.Genders.CountAsync().Result);
            Assert.AreEqual(1, db.Users.CountAsync().Result);
            Assert.AreEqual(0, db.UserDataMaps.CountAsync().Result);

            map.SingleReference<Gender>().SetAsync(user, db.Genders.FirstAsync(g => g.Description == "Male").Result).Wait();
            Assert.AreEqual(1, db.UserDataMaps.CountAsync().Result);

            var mapping = map.SingleReference<Gender>().GetMappingInfoOrNullAsync(user).Result;
            Assert.AreEqual(1, mapping.UserId);
            Assert.AreEqual(1, mapping.DataId);
            Assert.AreEqual(1, mapping.DataType);

            var gender = map.SingleReference<Gender>().GetOrDefaultAsync(user).Result;
            Assert.AreEqual("Male", gender.Description);

            // Change it's value, and make sure it worked properly.
            map.SingleReference<Gender>().SetAsync(user, db.Genders.FirstAsync(g => g.Description == "Female").Result).Wait();
            Assert.AreEqual(1, db.UserDataMaps.CountAsync().Result, "The data provider is making another entry for some reason.");

            gender = map.SingleReference<Gender>().GetOrDefaultAsync(user).Result;
            Assert.AreEqual("Female", gender.Description);

            // Perform a reverse lookup on the gender entity.
            var lookupId = map.SingleReference<Gender>().ReverseLookupUserId(gender).Result;
            Assert.AreEqual(user.UserId, lookupId);
        }
    }
}
