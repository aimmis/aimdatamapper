﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace AimDataMapper.Tests
{
    class TestEntityIds
    {
        public static int Gender => 1;
        public static int NameInfo => 2;
    }

    class TestContext : DbContext
    {
        public TestContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<NameInfo> NameInfos { get; set; }
        public DbSet<UserDataMap> UserDataMaps { get; set; }
    }

    class User
    {
        public int UserId { get; set; }
    }

    class Gender
    {
        public int GenderId { get; set; }
        public string Description { get; set; }
    }

    class NameInfo
    {
        public int NameInfoId { get; set; }
        public string First { get; set; }
        public string Second { get; set; }
    }

    class UserDataMap
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int UserDataMapId { get; set; }

        public User User { get; set; }
        public int UserId { get; set; }

        public int DataType { get; set; }
        public int DataId { get; set; }
    }
}
